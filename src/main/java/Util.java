
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Util {

    static int last = 0;
    static Scanner scanner = new Scanner(System.in);

    static void Display(Connection connection, int pageSize, int currentPage) {
        List<Product> products = new ArrayList<>();
        try {
            String select = "Select * from product";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(select);
            while (rs.next()) {
                products.add(new Product(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getDate(5).toLocalDate()));
            }
            displayProduct(products, "List of Product", currentPage, pageSize);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void searchById(Connection connection){
        System.out.print("Enter ID:");
        int id=validateUserInputString();
        String serchById="SELECT * FROM product where id="+id;
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        Table t = new Table(1, BorderStyle.DESIGN_TUBES_WIDE, ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        t.addCell("PRODUCT");
        try {
            PreparedStatement stm=connection.prepareStatement(serchById);
            ResultSet rs=stm.executeQuery();
            while (rs.next()){
                t.addCell("ID: "+rs.getInt(1));
                t.addCell("Name: "+rs.getString(2));
                t.addCell("Price: "+rs.getDouble(3));
                t.addCell("QTY: "+rs.getInt(4));
                t.addCell("Date: "+rs.getDate(5));
            }
        } catch (SQLException e) {

        }
        System.out.println(t.render());
    }
    // Validate user input number
    public static String validateUserInputNumber() {
        Pattern pattern = Pattern.compile("^[a-zA-Z]+( [a-zA-Z]+)*$");
        boolean check;
        String inputText;
        do {
//            System.out.print();
            inputText = scanner.nextLine();
            check = pattern.matcher(inputText.trim()).matches();
            if (!check)
                System.out.print( "Invalid!! Name must be Text \n\t=>Enter Again:" );
        } while (!check);
        return inputText;
    }

    //----------- Validate user in put string
    public static int validateUserInputString() {
        Scanner scan = new Scanner(System.in);
        String number;
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher;
        do {
            number = scan.next();
            matcher = pattern.matcher(number);

            if (matcher.matches() == false)
                System.out.print("\t Invalid! Please input number: ");
        } while (matcher.matches() != true);
        return Integer.parseInt(number);

    }

    static Product getInputFromUser() {
        LocalDate localDate = LocalDate.now();
        System.out.print("Enter name: ");
        String name = validateUserInputNumber();
        System.out.print("Enter price: ");
        double price = validateUserInputString();
        System.out.print("Enter quantity: ");
        int qty = validateUserInputString();
        System.out.println(localDate);
        return new Product(name, price, qty, localDate);
    }

    static void unUpdate(Connection connection, List<Product> products) {
        LocalDate localDate = LocalDate.now();
        System.out.print("Enter id:");
        int id = validateUserInputString();
        try {
            String search = "select * from product where id=" + id;
            PreparedStatement statement = connection.prepareStatement(search);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                System.out.print("Enter name: ");
                String name = validateUserInputNumber();
                System.out.print("Enter price: ");
                double price = validateUserInputString();
                System.out.print("Enter quantity: ");
                int qty = validateUserInputString();
                System.out.println(localDate);
                products.add(new Product(rs.getInt(1), name, price, qty, localDate));
            }
            if (!rs.next()) {
                System.out.println("Not fount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    static void deleteQuery(Connection connection) {
        List<Product> products=new ArrayList<>();
        System.out.print("Enter id");
        int id=validateUserInputString();
        String serchById="SELECT * FROM product where id="+id;
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        Table t = new Table(5, BorderStyle.DESIGN_TUBES_WIDE, ShownBorders.HEADER_AND_COLUMNS);
        try {
            PreparedStatement stm=connection.prepareStatement(serchById);
            ResultSet rs=stm.executeQuery();
            while (rs.next()){
                products.add(new Product(rs.getInt(1),rs.getString(2),rs.getDouble(3),rs.getInt(4), rs.getDate(5).toLocalDate()));
            }
            displayPro(products);
        } catch (SQLException e) {

        }
        System.out.println("Do you to delete? Press Y to delete N to Cancel");
        String p=validateUserInputNumber().toLowerCase();
        if (p.equals("y")){
            try {
                String delete = "DELETE from  product where id=?";
                PreparedStatement preparedStatement = connection.prepareStatement(delete);
                preparedStatement.setInt(1, id);
                int row = preparedStatement.executeUpdate();
                if (row > 0) {
                    System.out.println( " Successfully Delete Data " );
                } else {
                    System.out.println( "Failed...!!" );
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else {
            System.out.println("Cancel....");
        }

    }

    public static void unSave(List<Product> products) {
        Product p = getInputFromUser();
        products.add(p);
    }

    static void displayUnsave(List<Product> products, String title) {
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        Table t = new Table(4, BorderStyle.DESIGN_TUBES_WIDE, ShownBorders.ALL);
        t.addCell(title, numberStyle, 4);
        t.addCell("Name", numberStyle);
        t.addCell("Unit Price", numberStyle);
        t.addCell("Stock Quantity", numberStyle);
        t.addCell("Imported Date", numberStyle);
        for (Product product : products) {
            t.addCell(product.getPro_name(), numberStyle);
            t.addCell(String.valueOf(product.getUnit_price()), numberStyle);
            t.addCell(String.valueOf(product.getQty()), numberStyle);
            t.addCell(String.valueOf(product.getDate()), numberStyle);
        }

        System.out.println(t.render());
    }

    public static void Search(Connection connection) {
        String name;
        System.out.print("Input Name to Search: ");
        name = scanner.next();
        String searchQuery = "SELECT * FROM product where name ILIKE '%" + name + "%'";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(searchQuery);
            ResultSet rs = preparedStatement.executeQuery();
            CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
            Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
            t.addCell("ID", numberStyle);
            t.addCell("Name", numberStyle);
            t.addCell("Unit Price", numberStyle);
            t.addCell("Stock Quantity", numberStyle);
            t.addCell("Imported Date", numberStyle);
            while (rs.next()) {
                t.addCell(String.valueOf(rs.getInt(1)), numberStyle);
                t.addCell(rs.getString(2), numberStyle);
                t.addCell(String.valueOf(rs.getFloat(3)), numberStyle);
                t.addCell(String.valueOf(rs.getInt(4)), numberStyle);
                t.addCell(String.valueOf(rs.getDate(5)), numberStyle);
            }
            System.out.println(t.render());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void displayPro(List<Product> products) {
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        Table t = new Table(5, BorderStyle.DESIGN_TUBES_WIDE, ShownBorders.ALL);
        t.addCell("ID", numberStyle);
        t.addCell("Name", numberStyle);
        t.addCell("Unit Price", numberStyle);
        t.addCell("Stock Quantity", numberStyle);
        t.addCell("Imported Date", numberStyle);
        for (Product p : products) {
            t.addCell("" + p.getId(), numberStyle);
            t.addCell(p.getPro_name(), numberStyle);
            t.addCell("" + p.getUnit_price(), numberStyle);
            t.addCell("" + p.getQty(), numberStyle);
            t.addCell("" + p.getDate(), numberStyle);

        }
        System.out.println(t.render());
    }


    static void insert(Connection connection, List<Product> products) {
        String insert = "Insert into product values(default,?,?,?,?)";
        int in = 0;

        try {
            for (int i = 0; i < products.size(); i++) {
                PreparedStatement statement = connection.prepareStatement(insert);
                statement.setString(1, products.get(i).getPro_name());
                statement.setDouble(2, products.get(i).getUnit_price());
                statement.setInt(3, products.get(i).getQty());
                statement.setDate(4, Date.valueOf(products.get(i).getDate()));
                in = statement.executeUpdate();
            }
            if (in > 0) {
                System.out.println("New Insterd");

            } else {
                System.out.println("Out jol");
            }
        } catch (SQLException e) {

        }

        products.removeAll(products);
    }

    static void update(Connection connection, List<Product> products) {
        String update = "update  product set pro_name=?,unit_price=?,quantity=?,import_date=? where id=?";
        int in = 0;
        try {
            for (int i = 0; i < products.size(); i++) {
                PreparedStatement statement = connection.prepareStatement(update);
                statement.setString(1, products.get(i).getPro_name());
                statement.setDouble(2, products.get(i).getUnit_price());
                statement.setInt(3, products.get(i).getQty());
                statement.setDate(4, Date.valueOf(products.get(i).getDate()));
                statement.setInt(5, products.get(i).getId());
                in = statement.executeUpdate();
            }
            if (in > 0) {
                System.out.println("Updated!!!");

            } else {
                System.out.println("Out jol");
            }
        } catch (SQLException e) {

        }

        products.removeAll(products);

    }

    static void displayProduct(List<Product> products, String title, int currentPage, int pageSize) {
        int pageOf = 0;
        if (products.size() % pageSize == 0) {
            pageOf = products.size() / pageSize;
        } else {
            pageOf = (products.size() / pageSize) + 1;
        }
        last = pageOf;
        List<Product> list = pagination(products, currentPage, pageSize);
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        Table t = new Table(5, BorderStyle.DESIGN_TUBES_WIDE, ShownBorders.ALL);
        t.addCell(title, numberStyle, 5);
        t.addCell("ID", numberStyle);
        t.addCell("Name", numberStyle);
        t.addCell("Unit Price", numberStyle);
        t.addCell("Stock Quantity", numberStyle);
        t.addCell("Imported Date", numberStyle);
        for (Product product : list) {
            t.addCell(String.valueOf(product.getId()), numberStyle);
            t.addCell(product.getPro_name(), numberStyle);
            t.addCell(String.valueOf(product.getUnit_price()), numberStyle);
            t.addCell(String.valueOf(product.getQty()), numberStyle);
            t.addCell(String.valueOf(product.getDate()), numberStyle);
        }
        t.addCell(String.valueOf(currentPage) + "of" + String.valueOf(pageOf), numberStyle, 2);
        t.addCell("Total " + String.valueOf(products.size()) + " records", numberStyle, 3);

        System.out.println(t.render());
    }

    //static  void
    static List<Product> pagination(List<Product> products, int currentPage, int pageSize) {
        int index = (currentPage - 1) * pageSize;
        return products.stream().skip(index)
                .limit(pageSize).collect(Collectors.toList());
    }
}
