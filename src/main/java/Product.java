
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class Product implements Serializable {
    private int id;
    private String pro_name;
    private double unit_price;
    private int qty;
    private LocalDate date;

    public Product() {
    }

    public Product(String pro_name, double unit_price, int qty, LocalDate date) {
        this.pro_name = pro_name;
        this.unit_price = unit_price;
        this.qty = qty;
        this.date = date;
    }

    public Product(int id, String pro_name, double unit_price, int qty, LocalDate date) {
        this.id = id;
        this.pro_name = pro_name;
        this.unit_price = unit_price;
        this.qty = qty;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(double unit_price) {
        this.unit_price = unit_price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", pro_name='" + pro_name + '\'' +
                ", unit_price=" + unit_price +
                ", qty=" + qty +
                ", date=" + date +
                '}';
    }
}
