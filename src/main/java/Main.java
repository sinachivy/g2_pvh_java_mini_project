

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {


    static Scanner scanner = new Scanner(System.in);
    static int pageSize = 3;
    static int currentPage = 1;
    public static void main(String[] args) {
        ConnectionDB connectionDB = new ConnectionDB();
        Connection connection = connectionDB.getConnection();
        List<Product> products = new ArrayList<>();
        List<Product> upProducts = new ArrayList<>();

        String option;
        do {
            Util.Display(connection,pageSize,currentPage);
            //menu

            System.out.println("\n\n*)Display");
            System.out.println("----------------------------------------------------------------------------------------------------------------------------------");
            System.out.println("|   W)Write      | R) Read    | U)Update     | D) Delete    |S) Search    | F) First      | P) Previous   | N) Next           \n");
            System.out.println("|   L) Last      | G) Goto    | Se)Set Row   | Sa) Save     | Un) Unsave  | Ba) Back up   | Re) Restore   | E) Exit   ");
            System.out.println("----------------------------------------------------------------------------------------------------------------------------------");


            System.out.print( "\nPlease Chooser your option:" );
            option = Util.validateUserInputNumber();
            switch (option) {
                case "w": {
                    System.out.println("----- Write -----");
                    Util.unSave(products);
                }
                break;
                case "r": {
                    Util.searchById(connection);
                }
                break;
                case "u": {
                    System.out.println("----- Update -----");
                    Util.unUpdate(connection,upProducts);
                }
                break;
                case "d": {
                    System.out.println("----- Delete -----");
                    Util.deleteQuery(connection);
                }
                break;
                case "s": {
                    System.out.println("----- Search -----");
                    Util.Search(connection);
                }
                break;
                case "f": {
                    System.out.println("----- First Page -----");
                }
                break;
                case "p": {
                    System.out.println("----- Previous Page -----");
                    currentPage--;
                    if (currentPage<=0){
                        currentPage=1;
                    }

                }
                break;
                case "n": {
                    System.out.println("----- Next Page -----");
                    currentPage++;
                    if (currentPage>Util.last){
                        currentPage=Util.last;
                    }
                }
                break;
                case "l": {
                    System.out.println("----- Last Page -----");
                    currentPage=Util.last;
                }
                break;
                case "g": {
                    System.out.println("----- Goto Page -----");
                    System.out.print("Enter page:");
                    currentPage=Util.validateUserInputString();
                }
                break;
                case "sa": {
                    System.out.println("----- Save Data -----");
                    if (products.size()>0){
                        Util.insert(connection,products);
                    }
                    if (upProducts.size()>0){
                        Util.update(connection,upProducts);
                    }
                }
                break;
                case "un": {
                    String o;
                    do {
                        System.out.print("I Unsave Insert U Unsave upadated and B back");
                        o= Util.validateUserInputNumber().toLowerCase();
                        switch (o) {
                            case "i": {
                                Util.displayUnsave(products,"Unsave Instert");
                                break;
                            }
                            case "u": {
                                Util.displayUnsave(upProducts,"Unsave Update");
                                break;
                            } case "b":
                            {
                                o="ex";
                            }
                        }
                    } while (o != "ex");
                }
                break;
                case "ba": {
                    System.out.println("----- Backup Data -----");
                }
                break;
                case "re": {
                    System.out.println("----- Restore Data -----");
                }
                break;
                case "se": {
                    System.out.println("----- Set Row -----");
                    System.out.print("Enter row number:");
                    pageSize=Util.validateUserInputString();
                }
                break;
                case "e": {
                    System.out.println("----- Exit From Program -----");
                    System.exit(0);
                }
                break;
                default:
                    System.out.println(" ");
            }
        } while (option != "e");
    }

}